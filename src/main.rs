mod map;
mod partitiontree;

use image::{ImageBuffer, ImageFormat, Rgb};
use noise::{NoiseFn, Perlin, Seedable};
use rand::Rng;

use map::{Map, Terrain};
use partitiontree::{PartitionTree, Pos};

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
struct Human {
    strength: u8,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum Occupant {
    Human(Human),
    Zombie,
}

fn probabilities(terrain: Terrain) -> f64 {
    let density_miles = match terrain {
        Terrain::City => 5000.,
        Terrain::Village => 1500.,
        Terrain::Forest => 1.,
        Terrain::Plain => 80.,
        Terrain::Road => 50.,
        Terrain::Water => 0.,
    }; // Per square mile
    let density_m = density_miles / 2.59e6;
    density_m * 100.
}

impl Human {
    pub fn random<R: Rng>(rng: &mut R) -> Self {
        Human {
            strength: rng.gen_range(30, 70),
        }
    }
}

fn render<Container>(
    pop: &PartitionTree<Occupant>,
    img: &ImageBuffer<Rgb<u8>, Container>,
) -> ImageBuffer<Rgb<u8>, Container>
where
    Container: std::ops::Deref<Target = [u8]> + std::ops::DerefMut + Clone,
{
    let mut dens_img = img.clone();
    for (val, Pos { x, y }) in pop.iter() {
        dens_img.put_pixel(
            *x,
            *y,
            match val {
                Occupant::Human(..) => Rgb([255; 3]), // Rgb([0xDF, 0x65, 0x89]),
                Occupant::Zombie => Rgb([255, 0, 0]),
            },
        );
    }
    dens_img
}

fn main() {
    let terrain = Map::gen(1, 1);
    let s_w = 1;
    let s_h = 1;
    let start = std::time::Instant::now();
    let img = ImageBuffer::from_fn(terrain.size() / s_w, terrain.size() / s_h, |x, y| {
        let mut avg = [0; 3];
        for dx in 0..s_w {
            for dy in 0..s_h {
                let v = terrain.get(s_w * x + dx, s_w * y + dy).to_pixel();
                avg[0] += v.0[0];
                avg[1] += v.0[1];
                avg[2] += v.0[2];
            }
        }
        Rgb(avg)
    });
    println!("Image gen: {:?}", start.elapsed());
    let start = std::time::Instant::now();
    img.save_with_format("map.tiff", ImageFormat::Tiff).unwrap();
    println!("Image save: {:?}", start.elapsed());
    let mut individuals = Vec::with_capacity(300);
    let mut pop = PartitionTree::with_capacity(terrain.size() as usize - 1, 16, 10);
    let mut rng = rand::thread_rng();
    for x in 0..terrain.size() - 1 {
        for y in 0..terrain.size() - 1 {
            let density = probabilities(terrain.get(x, y));
            if rng.gen_range(0., 1.) <= density {
                individuals.push(Occupant::Human(Human::random(&mut rng)));
                pop.add(Pos { x, y }, Occupant::Human(Human::random(&mut rng)));
            }
        }
    }

    let mut last_change = 0;
    let tot = pop.count();
    let start_poses: Vec<_> = std::iter::repeat_with(|| pop.set_rand(Occupant::Zombie, &mut rng))
        .take(10)
        .collect();
    let mut p_zombie = 1;
    let mut dead = 0;
    let mut step = 0;
    let mut set_zombie = Vec::default();
    let mut kill = Vec::default();
    let mut pulls = Vec::with_capacity(tot);
    let mut start_img = render(&pop, &img);
    for start_pos in start_poses {
        let start_x = start_pos.x.saturating_sub(30);
        let start_y = start_pos.y.saturating_sub(30);
        let end_x = (start_pos.x + 30).min(terrain.size() - 1);
        let end_y = (start_pos.y + 30).min(terrain.size() - 1);
        for i in -30..30 {
            let x = (start_pos.x as i32 + i)
                .max(0)
                .min(terrain.size() as i32 - 1) as _;
            let y = (start_pos.y as i32 + i)
                .max(0)
                .min(terrain.size() as i32 - 1) as _;
            start_img.put_pixel(start_x, y, Rgb([255, 0, 0]));
            start_img.put_pixel(end_x, y, Rgb([255, 0, 0]));
            start_img.put_pixel(x, start_y, Rgb([255, 0, 0]));
            start_img.put_pixel(x, end_y, Rgb([255, 0, 0]));
        }
    }
    start_img
        .save_with_format("steps/initial.tiff", ImageFormat::Tiff)
        .unwrap();
    while last_change < 100 {
        for (val, Pos { x, y }) in pop.iter() {
            if *val == Occupant::Zombie {
                for i in 0..9 {
                    if i == 5 {
                        continue;
                    }
                    (|| {
                        let nx = x.checked_add(i % 3)?.checked_sub(1)?;
                        let ny = y.checked_add(i / 3)?.checked_sub(1)?;
                        let p = Pos { x: nx, y: ny };
                        if let Occupant::Human(h) = pop.get(p)? {
                            if rng.gen_range(0, 100) > h.strength {
                                set_zombie.push(p);
                            } else {
                                kill.push(p);
                            }
                        }
                        Some(())
                    })();
                }
            }
        }
        let new_zombie = set_zombie.len();
        let new_dead = kill.len();
        for p in set_zombie.drain(..) {
            pop.replace(p, Occupant::Zombie);
        }
        for p in kill.drain(..) {
            pop.remove(p);
        }
        let noise = Perlin::new();
        pulls.extend(pop.iter().enumerate().map(|(i, (p, pt))| {
            noise.set_seed(i as _);
            let smooth = 0.8;
            let mut pull = (noise.get([pt.x as f64 * smooth, pt.y as f64 * smooth])
                * std::f64::consts::PI)
                .sin_cos();
            if *p == Occupant::Zombie {
                for (_, pt2) in pop.iter_near(*pt, 100) {
                    let dst = pt.dst(pt2);
                    let dir = (pt.x as f64 - pt2.x as f64, pt.y as f64 - pt2.y as f64);
                    let dir_norm = (dir.0 * dir.0 + dir.1 * dir.1).sqrt();
                    let dir = (dir.0 / dir_norm, dir.1 / dir_norm);
                    let sound_pull = 40. * dst.powf(-1.5);
                    pull.0 += sound_pull * dir.0;
                    pull.1 += sound_pull * dir.1;
                }
            }
            let pull_norm = (pull.0 * pull.0 + pull.1 * pull.1).sqrt();
            if pull_norm > 1. {
                pull = (pull.0 / pull_norm, pull.1 / pull_norm);
            }
            // if x² + y² == 1, |x| and |y| can't both be less than 0.5
            Pos {
                x: (pt.x + pull.0.round() as u32).min(terrain.size() - 1),
                y: (pt.y + pull.1.round() as u32).min(terrain.size() - 1),
            }
        }));
        pop.move_all(pulls.drain(..));

        if step % 1 == 0 {
            render(&pop, &img)
                .save_with_format(format!("steps/step-{}.tiff", step), ImageFormat::Tiff)
                .unwrap();
        }
        step += 1;
        if new_dead == 0 && new_zombie == 0 {
            last_change += 1;
        } else {
            last_change = 0;
            p_zombie += new_zombie;
            dead += new_dead;
        }
        println!(
            "Step {}: {}% healty, {}% dead",
            step,
            100. * (1. - p_zombie as f64 / tot as f64),
            100. * dead as f64 / tot as f64
        );
    }
}
