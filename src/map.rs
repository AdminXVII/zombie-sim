use embedded_graphics::{
    pixelcolor::raw::RawU8, prelude::*, primitives::Line, style::PrimitiveStyle,
};
use image::Rgb;
use noise::{NoiseFn, Perlin};
use rand::Rng;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Terrain {
    City,
    Plain,
    Forest,
    Road,
    Village,
    Water,
}

pub struct Map {
    tiles: Box<[Terrain]>,
    nodes: Vec<(u32, u32)>,
    size: u32,
}

impl PixelColor for Terrain {
    type Raw = RawU8;
}

impl DrawTarget<Terrain> for Map {
    type Error = std::convert::Infallible;

    fn draw_pixel(&mut self, item: Pixel<Terrain>) -> Result<(), Self::Error> {
        if item.0.x < self.size as i32
            && item.0.y < self.size as i32
            && item.0.x >= 0
            && item.0.y >= 0
        {
            self.set(item.0.x as u32, item.0.y as u32, item.1);
        }
        Ok(())
    }

    fn size(&self) -> Size {
        Size::new(self.size, self.size)
    }
}

impl Terrain {
    pub fn to_pixel(self) -> Rgb<u8> {
        match self {
            Self::Plain => Rgb([0xB2, 0x85, 0x51]), // Rgb([0xF3, 0xE7, 0xA9]),
            Self::Forest => Rgb([0x06, 0x51, 0x31]),
            Self::Water => Rgb([0x0E, 0xBD, 0xDD]),
            Self::City => Rgb([0x3A, 0x3D, 0x32]),
            Self::Village => Rgb([0x47, 0x2C, 0x1F]), // Rgb([0xC2, 0x81, 0x43]),
            Self::Road => Rgb([0xA2, 0xAD, 0xAF]),
        }
    }
}

fn square_step<R: Rng>(
    tiles: &mut [u8],
    x: usize,
    y: usize,
    w: usize,
    reach: usize,
    scale: i16,
    rng: &mut R,
) {
    let mut avg = 0_i16;
    avg += tiles[(x - reach) * w + y - reach] as i16;
    avg += tiles[(x - reach) * w + y + reach] as i16;
    avg += tiles[(x + reach) * w + y - reach] as i16;
    avg += tiles[(x + reach) * w + y + reach] as i16;
    avg += rng.gen_range(-(reach as i16) * scale, reach as i16 * scale);
    avg /= 4;
    tiles[x * w + y] = i16::min(avg, 255) as u8;
}

fn diamond_step<R: Rng>(
    tiles: &mut [u8],
    x: usize,
    y: usize,
    w: usize,
    reach: usize,
    scale: i16,
    rng: &mut R,
) {
    let mut count = 0;
    let mut avg = 0_i16;
    if x >= reach {
        avg += tiles[(x - reach) * w + y] as i16;
        count += 1;
    }
    if x + reach < w {
        avg += tiles[(x + reach) * w + y] as i16;
        count += 1;
    }
    if y >= reach {
        avg += tiles[x * w + y - reach] as i16;
        count += 1;
    }
    if y + reach < w {
        avg += tiles[x * w + y + reach] as i16;
        count += 1;
    }
    avg += rng.gen_range(-(reach as i16) * scale, reach as i16 * scale);
    avg /= count;
    tiles[x * w + y] = i16::min(avg, 255) as u8;
}

fn diamond_square<R: Rng>(tiles: &mut [u8], w: usize, scale: i16, rng: &mut R) {
    tiles[0] = rng.gen();
    tiles[w - 1] = rng.gen();
    tiles[tiles.len() - w] = rng.gen();
    tiles[tiles.len() - 1] = rng.gen();
    let mut size = w;
    let h = tiles.len() / w;
    while size > 1 {
        for x in (size / 2..w).into_iter().step_by(size) {
            for y in (size / 2..h).into_iter().step_by(size) {
                square_step(tiles, x, y, w, size / 2, scale, rng);
            }
        }
        for (col, x) in (0..w).into_iter().step_by(size / 2).enumerate() {
            let start = ((col + 1) % 2) * (size / 2);
            for y in (start..h).into_iter().step_by(size) {
                diamond_step(tiles, x, y, w, size / 2, scale, rng);
            }
        }
        size >>= 1;
    }
}

fn dst(x: u32, y: u32, pt: (u32, u32), max: u32) -> f64 {
    let d = dst_abs((x, y), pt);
    d / max as f64 / (2_f64).sqrt()
}

fn dst_abs(p1: (u32, u32), p2: (u32, u32)) -> f64 {
    let dx = (p1.0 as i32 - p2.0 as i32).abs() as f64;
    let dy = (p1.1 as i32 - p2.1 as i32).abs() as f64;
    (dx * dx + dy * dy).sqrt()
}

fn near<R: Rng>(pt: (u32, u32), dst: i32, rng: &mut R) -> (u32, u32) {
    let dx = rng.gen_range(-dst, dst as i32);
    let dy = rng.gen_range(-dst, dst as i32);
    ((pt.0 as i32 + dx) as u32, (pt.1 as i32 + dy) as u32)
}

fn join(
    noise: &Perlin,
    terrain: &mut Map,
    nodes: &mut Vec<(usize, (u32, u32))>,
    cluster: usize,
    mut p: (u32, u32),
    target_p: (u32, u32),
) {
    let mut count = 0;
    loop {
        let width = match terrain.get(p.0, p.1) {
            Terrain::City => 5,
            Terrain::Village => 3,
            Terrain::Plain => 10,
            Terrain::Forest => 4,
            Terrain::Road => 5,
            Terrain::Water => unreachable!(),
        };
        let dst = dst_abs(target_p, p);
        let length: f64 = match terrain.get(p.0, p.1) {
            Terrain::City => 1.,
            Terrain::Village => 2.,
            Terrain::Plain => 50.,
            Terrain::Forest => 10.,
            Terrain::Road => 13.,
            Terrain::Water => unreachable!(),
        };
        let target = if dst < length {
            target_p
        } else {
            let adj = 0.2;
            let n1 = noise.get([p.0 as f64 * adj, p.1 as f64 * adj]);
            let n2 = noise.get([p.0 as f64 * adj + 0.5, p.1 as f64 * adj + 0.5]);
            let m = f64::min(30., (target_p.0 as f64 - p.0 as f64) / 2.);
            let m = f64::min(m, (target_p.1 as f64 - p.1 as f64) / 2.);
            let v = (
                target_p.0 as i32 - p.0 as i32 + (n1 * m) as i32,
                target_p.1 as i32 - p.1 as i32 + (n2 * m) as i32,
            );
            let mut dx = (length * v.0 as f64 / dst) as i32;
            let mut dy = (length * v.1 as f64 / dst) as i32;
            // Ensure that the dot moves (rounding?)
            if dx == 0 && dy == 0 {
                if v.0.abs() > v.1.abs() {
                    dx += v.0.signum();
                } else {
                    dy += v.1.signum();
                }
            }
            if count > 1000 {
                println!(
                    "Stuck in loop: {:?} to {:?}, dx: {}, dy: {}",
                    p, target_p, dx, dy
                );
                if count > 1010 {
                    panic!()
                }
            }
            count += 1;
            (
                (p.0 as i32 + dx).max(0).min(terrain.size as i32 - 1) as u32,
                (p.1 as i32 + dy).max(0).min(terrain.size as i32 - 1) as u32,
            )
        };
        Line::new(
            Point::new(p.0 as _, p.1 as _),
            Point::new(target.0 as _, target.1 as _),
        )
        .into_styled(PrimitiveStyle::with_stroke(Terrain::Road, width))
        .draw(terrain)
        .unwrap();
        nodes.push((cluster, p));
        if p == target {
            break;
        }
        p = target;
        let c = nodes
            .iter()
            .find(|p| p.1 == target_p)
            .expect("No target?")
            .0;
        if c != cluster {
            // Join clusters
            for p in nodes.iter_mut() {
                if p.0 == c {
                    p.0 = cluster;
                }
            }
        }
    }
}

impl Map {
    pub fn gen(num_squares: u32, scale: i16) -> Self {
        if num_squares & (num_squares - 1) != 0 || num_squares == 0 {
            panic!("Map generation can only generate numbers of squares that are powers of two");
        }
        let size = (1 << 10) * num_squares + 1;
        let num_city = num_squares;
        let num_villages = num_squares * 10;
        let max_city_size = 300;
        let max_village_size = 50;

        let start = std::time::Instant::now();
        let mut terrain = Vec::with_capacity((size * size) as usize);

        let mut rng = rand::thread_rng();
        let mut elevation = vec![0; (size * size) as usize];
        let mut temp = vec![0; (size * size) as usize];
        diamond_square(&mut elevation, size as usize, scale, &mut rng);
        diamond_square(&mut temp, size as usize, scale, &mut rng);
        for i in 0..size * size {
            terrain.push(match (elevation[i as usize], temp[i as usize]) {
                (e, _) if e < 70 => Terrain::Water,
                (_, t) if t > 150 => Terrain::Forest,
                _ => Terrain::Plain,
            });
        }
        std::mem::drop((temp, elevation));
        let mut terrain = Self {
            tiles: terrain.into_boxed_slice(),
            nodes: Vec::default(),
            size,
        };
        let mut points = Vec::with_capacity((num_city + num_villages) as usize);

        for _ in 0..num_city {
            let noise = Perlin::new();
            let city_center = terrain.sample(&mut rng);
            let c1 = near(city_center, max_city_size as i32, &mut rng);
            let c2 = near(city_center, max_city_size as i32, &mut rng);
            let smooth = 3.0 / max_city_size as f64;
            points.push(city_center);
            for x in city_center.0.saturating_sub(max_city_size)
                ..u32::min(city_center.0 + max_city_size, size)
            {
                for y in city_center.1.saturating_sub(max_city_size)
                    ..u32::min(city_center.1 + max_city_size, size)
                {
                    let d = dst(x, y, city_center, max_city_size);
                    let d1 = dst(x, y, c1, max_city_size);
                    let d2 = dst(x, y, c2, max_city_size);
                    let v = noise.get([x as f64 * smooth, y as f64 * smooth]) / 2. + 0.5;
                    let p = [0.2, 0.9, 1.];
                    let m = (v * p[0] + (1. - d) + p[1] * (1. - d1) + p[2] * (1. - d2))
                        / (p[0] + p[1] + p[2] + 1.);
                    if terrain.get(x, y) == Terrain::Plain && m > 0.5 {
                        terrain.set(x, y, Terrain::City);
                    }
                }
            }
            println!("Generated city");
        }
        println!("Generating villages");
        for _ in 0..num_villages {
            let noise = Perlin::new();
            let city_center = terrain.sample(&mut rng);
            let smooth = 0.8 / max_village_size as f64;
            points.push(city_center);
            for x in city_center.0.saturating_sub(max_village_size)
                ..u32::min(city_center.0 + max_village_size, size)
            {
                for y in city_center.1.saturating_sub(max_village_size)
                    ..u32::min(city_center.1 + max_village_size, size)
                {
                    let d = dst(x, y, city_center, max_village_size);
                    let v = noise.get([x as f64 * smooth, y as f64 * smooth]) / 2. + 0.5;
                    let p = 0.8;
                    let m = (v * p + (1. - d)) / (p + 1.);
                    if terrain.get(x, y) == Terrain::Plain && m > 0.5 {
                        terrain.set(x, y, Terrain::Village);
                    }
                }
            }
            println!("Generated village");
        }
        println!("Generated all villages");
        let mut nodes = Vec::with_capacity(200);
        nodes.extend(points.iter().copied().enumerate());
        let noise = Perlin::new();
        while let Some(target_p) = points.pop() {
            let (cluster, p) = *nodes
                .iter()
                .filter(|&&p| p.1 != target_p)
                .min_by(|p1, p2| {
                    dst(target_p.0, target_p.1, p1.1, size)
                        .partial_cmp(&dst(target_p.0, target_p.1, p2.1, size))
                        .expect("NaN distance?")
                })
                .unwrap();
            join(&noise, &mut terrain, &mut nodes, cluster, p, target_p);
        }
        while nodes.iter().any(|n| nodes[0].0 != n.0) {
            let mut min_dst = std::f64::INFINITY;
            let (mut n1, mut n2) = (nodes[0], nodes[1].1);
            let mut found = false;
            for (i, &(cluster, p1)) in nodes.iter().enumerate() {
                for &(c, p2) in &nodes[i..] {
                    if c == cluster {
                        continue;
                    }
                    let dst = dst_abs(p1, p2);
                    if dst < min_dst {
                        min_dst = dst;
                        n1 = (cluster, p1);
                        n2 = p2;
                        found = true;
                    }
                }
            }
            if !found {
                panic!("Did not find a pair to join");
            }
            join(&noise, &mut terrain, &mut nodes, n1.0, n1.1, n2);
        }
        println!("Terrain gen: {:?}", start.elapsed());
        terrain.nodes.extend(nodes.into_iter().map(|p| p.1));
        terrain
    }

    pub fn get(&self, x: u32, y: u32) -> Terrain {
        self.tiles[(x * self.size + y) as usize]
    }

    pub fn set(&mut self, x: u32, y: u32, t: Terrain) {
        self.tiles[(x * self.size + y) as usize] = t;
    }

    fn sample<R: Rng>(&self, rng: &mut R) -> (u32, u32) {
        std::iter::repeat_with(|| {
            let x = rng.gen_range(0, self.size);
            let y = rng.gen_range(0, self.size);
            (x, y)
        })
        .take(self.size as usize * 20)
        .find(|&(x, y)| {
            if x == 0 || y == 0 || x == self.size - 1 || y == self.size - 1 {
                // println!("On edges");
                return false;
            }
            let mut nearby_water = 0;
            if self.get(x, y) != Terrain::Plain {
                // println!("Wrong terrain: {:?}", self.get(x, y));
                return false;
            }
            for dx in x - 1..x + 1 {
                for dy in y - 1..y + 1 {
                    if self.get(dx, dy) == Terrain::Water {
                        nearby_water += 1;
                    } else if self.get(dx, dy) != Terrain::Plain {
                        // println!("Wrong terrain nearby: {:?}", self.get(x, y));
                        return false;
                    }
                }
            }
            if nearby_water == 0 || nearby_water >= 4 {
                // println!("Wrong water terrain count: {}", nearby_water);
                return false;
            }
            true
        })
        .expect("Could not generate samples: too many tries")
    }

    pub fn size(&self) -> u32 {
        self.size
    }
}
