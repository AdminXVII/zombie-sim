use rand::{seq::SliceRandom, Rng};
use retain_mut::RetainMut;

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct Pos {
    pub x: u32,
    pub y: u32,
}

pub struct PartitionTree<O> {
    num_squares: usize,
    square_size: usize,
    squares: Box<[Vec<(O, Pos)>]>,
}

impl Pos {
    pub fn dst(&self, other: &Pos) -> f64 {
        let dx = (self.x - other.x) as f64;
        let dy = (self.y - other.y) as f64;
        (dx * dx + dy * dy).sqrt()
    }
}

impl<O: Clone> PartitionTree<O> {
    pub fn new(size: usize, square_size: usize) -> Self {
        Self::with_capacity(size, square_size, 0)
    }

    pub fn with_capacity(size: usize, square_size: usize, capacity: usize) -> Self {
        if size % square_size != 0 {
            panic!(
                "Size is not a multiple of square_size: {} % {} ≠ 0",
                size, square_size
            );
        }
        let num_squares = size / square_size;
        Self {
            num_squares,
            square_size,
            squares: vec![Vec::with_capacity(capacity); num_squares * num_squares]
                .into_boxed_slice(),
        }
    }

    pub fn replace(&mut self, pos: Pos, new: O) {
        let square = &mut self.squares[self.square(pos)];
        if let Some(v) = square.iter_mut().find(|&&mut (_, p)| p == pos) {
            v.0 = new;
        }
    }

    pub fn remove(&mut self, pos: Pos) {
        let square = &mut self.squares[self.square(pos)];
        if let Some(i) = square.iter().position(|&(_, p)| p == pos) {
            square.swap_remove(i);
        }
    }

    pub fn add(&mut self, pos: Pos, new: O) {
        let square = &mut self.squares[self.square(pos)];
        square.push((new, pos));
    }

    pub fn count(&self) -> usize {
        self.squares.iter().map(|s| s.len()).sum()
    }

    pub fn get(&self, pos: Pos) -> Option<&O> {
        let square = &self.squares[self.square(pos)];
        square.iter().find(|&(_, p)| *p == pos).map(|(v, _)| v)
    }

    pub fn set_rand<R: Rng>(&mut self, v: O, rng: &mut R) -> Pos
    where
        O: Eq,
    {
        loop {
            let square = self.squares.choose_mut(rng).unwrap();
            if let Some(s) = square.choose_mut(rng) {
                if s.0 != v {
                    s.0 = v;
                    return s.1;
                }
            }
        }
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = &'a (O, Pos)> + 'a {
        self.squares.iter().flat_map(|s| s.iter())
    }

    pub fn iter_near<'a>(&'a self, pos: Pos, dst: u32) -> impl Iterator<Item = &'a (O, Pos)> + 'a {
        let center = self.square(pos);
        let min_x = self.square(Pos {
            x: pos.x.saturating_sub(dst),
            y: pos.y,
        }) - center % self.square_size;
        let max_x = self.square(Pos {
            x: (pos.x + dst).min(self.square_size as _),
            y: pos.y,
        }) - center % self.square_size;
        let min_y = self.square(Pos {
            x: pos.x,
            y: pos.y.saturating_sub(dst),
        }) % self.square_size;
        let max_y = self.square(Pos {
            x: pos.x,
            y: (pos.y + dst).min(self.square_size as _),
        }) % self.square_size;
        (min_x..=max_x)
            .into_iter()
            .step_by(self.square_size)
            .flat_map(move |x| {
                (min_y..=max_y)
                    .into_iter()
                    .flat_map(move |y| self.squares[x + y].iter())
            })
    }

    fn square(&self, p: Pos) -> usize {
        let x = p.x as usize / self.square_size;
        let y = p.y as usize / self.square_size;

        x * self.num_squares + y
    }

    pub fn move_all<I: IntoIterator<Item = Pos>>(&mut self, iter: I)
    where
        O: Copy,
    {
        let mut to_move = Vec::new();
        let mut iter = iter.into_iter();
        for i in 0..self.squares.len() {
            let (before, after) = self.squares.split_at_mut(i);
            let square = &mut after[0];

            let square_size = self.square_size; // fix borrow checker
            let num_squares = self.num_squares; // ^
            square.retain_mut(|elem| {
                let new = iter.next().expect("Not enough element in iterator");
                // This is self.square, but the borrow checker did not agree
                let x = new.x as usize / square_size;
                let y = new.y as usize / square_size;

                let new_pos = x * num_squares + y;
                elem.1 = new;
                if new_pos == i {
                    true
                } else if new_pos > i {
                    to_move.push((new_pos, *elem));
                    false
                } else {
                    before[new_pos].push(*elem);
                    false
                }
            });

            let mut t = 0;
            while t != to_move.len() {
                if to_move[t].0 == i {
                    square.push(to_move.remove(t).1);
                } else {
                    t += 1;
                }
            }
        }
    }
}
