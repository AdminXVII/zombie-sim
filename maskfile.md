# Tasks

## run
```sh
rm -rf steps/*; cargo run --release
```

## video
```sh
ffmpeg -y -loop 1 -t 3 -i steps/initial.tiff -r 15 -i 'steps/step-%d.tiff' -filter_complex "[0:0] [1:0] concat=n=2:v=1:a=0" -c:v libx264 out.mp4 && mpv --loop out.mp4
```
